package com.ingesis.inxu.pryseg.dao;

import com.ingesis.inxu.pryseg.api.entity.PryEmpresaEntity;
import com.ingesis.inxu.pryseg.api.entity.PryProyectoEntity;

import javax.inject.Named;
import java.util.List;

@Named
public class ProyectoDAO extends AbstractPrySegDAO<PryProyectoEntity,Long> {

    public List<PryProyectoEntity> recuperarProyectosPorEmpresa(PryEmpresaEntity pryEmpresaEntity){
        QueryParameter queryParameter = QueryParameter.with("ideEmpresa",pryEmpresaEntity);
        return findWithNamedQuery("PryProyectoEntity.findAllByIdeEmpresa",
                queryParameter.parameters());
    }

    public List<PryProyectoEntity> recuperarTodosLosProyectos(){
        return findWithNamedQuery("PryProyectoEntity.findAll");
    }


}
