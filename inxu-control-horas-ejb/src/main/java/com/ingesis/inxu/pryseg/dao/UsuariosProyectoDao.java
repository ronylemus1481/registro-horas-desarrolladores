package com.ingesis.inxu.pryseg.dao;

import com.ingesis.inxu.pryseg.api.entity.PryUsuarioEntity;

import javax.inject.Named;
import java.util.List;

@Named
public class UsuariosProyectoDao extends AbstractPrySegDAO<PryUsuarioEntity,Long>{


    public List<PryUsuarioEntity> recuperarTodosLosUsuarios(){
        return findWithNamedQuery("PryUsuarioEntity.findAll");
    }
}
