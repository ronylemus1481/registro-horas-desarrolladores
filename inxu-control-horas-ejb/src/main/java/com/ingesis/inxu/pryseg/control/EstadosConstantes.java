package com.ingesis.inxu.pryseg.control;

public class EstadosConstantes {

    public final static String ACTIVO = "ACTIVO";
    public final static String INACTIVO = "INACTIVO";
    public final static String EN_PROCESO = "EN_PROCESO";

}
