package com.ingesis.inxu.pryseg.boundary;


import com.ingesis.inxu.pryseg.api.entity.PryRegistroHorasEntity;
import com.ingesis.inxu.pryseg.api.entity.PryUsuarioEntity;
import com.ingesis.inxu.pryseg.control.RegistroHoras;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

@Stateless(name = "registroHorasService")
@LocalBean
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class RegistroHorasService {

    @Inject
    private RegistroHoras registroHoras;


    public List<PryRegistroHorasEntity> recuperarRegistroHorasPorFecha(Date fechaInicial, Date fechaFinal){
        return registroHoras.recuperarRegistroHorasPorFecha(fechaInicial,fechaFinal);
    }

    public List<PryRegistroHorasEntity> recuperarRegistroHorasPorUsuarioFecha(Date fechaInicial, Date fechaFinal,
                                                                              PryUsuarioEntity usuarioEntity){
        return registroHoras.recuperarRegistroHorasPorUsuarioFecha(fechaInicial,fechaFinal,usuarioEntity);
    }

    public void crearRegistroHoras(PryRegistroHorasEntity registroHorasEntity){
        registroHoras.crearRegistroHoras(registroHorasEntity);
    }

    public void modificarRegistroHoras(PryRegistroHorasEntity registroHorasEntity){
        registroHoras.modificarRegistroHoras(registroHorasEntity);
    }

    public void eliminarRegistroHoras(PryRegistroHorasEntity registroHorasEntity){
        registroHoras.eliminarRegistroHoras(registroHorasEntity);
    }

}
