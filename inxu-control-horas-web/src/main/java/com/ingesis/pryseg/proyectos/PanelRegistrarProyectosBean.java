package com.ingesis.pryseg.proyectos;


import com.ingesis.inxu.pryseg.api.entity.PryEmpresaEntity;
import com.ingesis.inxu.pryseg.api.entity.PryProyectoEntity;
import com.ingesis.inxu.pryseg.boundary.EmpresaService;
import com.ingesis.inxu.pryseg.boundary.ProyectoService;
import com.ingesis.inxu.pryseg.control.EstadosConstantes;
import com.ingesis.inxu.pryseg.util.FacesUtil;
import com.ingesis.pryseg.comunes.ComunEstados;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ViewScoped
public class PanelRegistrarProyectosBean implements Serializable {

    Long ideProyecto;
    Long ideEmpresa;
    String codigoProyecto;
    String nombreProyecto;
    String estadoProyecto;
    BigDecimal horasCotizacion;
    BigDecimal valorCotizacion;

    private List<SelectItem> empresasOptions;
    private List<SelectItem> estadosOptions;

    @EJB
    private EmpresaService empresaService;
    @EJB
    private ProyectoService proyectoService;

    @ManagedProperty(value = "#{panelFiltroBusquedaProyectoBean}")
    private PanelFiltroBusquedaProyectoBean panelFiltroBusquedaProyectoBean;

    @PostConstruct
    public void init(){
        llenarListaEmpresas();
        llenarEstadosOptions();
    }


    public void llenarListaEmpresas() {
        List<PryEmpresaEntity> list = empresaService.recuperarEmpresasPorEstado(EstadosConstantes.ACTIVO);
        empresasOptions = new ArrayList<>();

        for (PryEmpresaEntity empresaEntity : list){
            empresasOptions.add(new SelectItem(empresaEntity.getIdeEmpresa(),empresaEntity.getCodigoEmpresa()
                    + "-"+empresaEntity.getNombreEmpresa()));
        }
    }

    private void llenarEstadosOptions(){
        estadosOptions = new ComunEstados().listaDeEstados();
    }

    public void agregarProyecto(){
        PryProyectoEntity proyectoEntity = new PryProyectoEntity();

        proyectoEntity.setPryEmpresaByIdeEmpresa((PryEmpresaEntity) empresaService.recuperarEmpresaPorReferencia(ideEmpresa));
        proyectoEntity.setCodigoProyecto(this.codigoProyecto);
        proyectoEntity.setNombreProyecto(this.nombreProyecto);
        proyectoEntity.setHorasCotizacion(this.horasCotizacion);
        proyectoEntity.setValorCotizacion(this.valorCotizacion);
        proyectoEntity.setEstadoProyecto(this.estadoProyecto);

        if (null != this.ideProyecto){
            proyectoEntity.setIdeProyecto(this.ideProyecto);
        }

        grabarProyecto(proyectoEntity);

        new FacesUtil().addInfoMessage("Se agregó el registro satisfactoriamente.");
    }

    public void inicializarPanelRegistroProyecto(){

    }

    private void grabarProyecto(PryProyectoEntity proyectoEntity){
        if (proyectoEntity.getIdeProyecto() == null){
            proyectoService.crearProyecto(proyectoEntity);
        }else{
            proyectoService.modificarProyecto(proyectoEntity);
        }

        panelFiltroBusquedaProyectoBean.llenarListaProyectos();
    }


    public void asignarProyectoParaModificar(PryProyectoEntity proyectoEntity){
        this.ideProyecto = proyectoEntity.getIdeProyecto();
        this.ideEmpresa = proyectoEntity.getPryEmpresaByIdeEmpresa().getIdeEmpresa();
        this.codigoProyecto = proyectoEntity.getCodigoProyecto();
        this.nombreProyecto = proyectoEntity.getNombreProyecto();
        this.horasCotizacion = proyectoEntity.getHorasCotizacion();
        this.valorCotizacion = proyectoEntity.getValorCotizacion();
        this.estadoProyecto = proyectoEntity.getEstadoProyecto();
    }

    public void eliminarProyecto(PryProyectoEntity proyectoEntity){
        proyectoService.eliminarProyecto(proyectoEntity);
        panelFiltroBusquedaProyectoBean.llenarListaProyectos();
    }

    public Long getIdeEmpresa() {
        return ideEmpresa;
    }

    public void setIdeEmpresa(Long ideEmpresa) {
        this.ideEmpresa = ideEmpresa;
    }

    public String getCodigoProyecto() {
        return codigoProyecto;
    }

    public void setCodigoProyecto(String codigoProyecto) {
        this.codigoProyecto = (codigoProyecto != null ? codigoProyecto.toUpperCase() : null);
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = (nombreProyecto != null ? nombreProyecto.toUpperCase() : null);
    }

    public BigDecimal getHorasCotizacion() {
        return horasCotizacion;
    }

    public void setHorasCotizacion(BigDecimal horasCotizacion) {
        this.horasCotizacion = horasCotizacion;
    }

    public BigDecimal getValorCotizacion() {
        return valorCotizacion;
    }

    public void setValorCotizacion(BigDecimal valorCotizacion) {
        this.valorCotizacion = valorCotizacion;
    }

    public String getEstadoProyecto() {
        return estadoProyecto;
    }

    public void setEstadoProyecto(String estadoProyecto) {
        this.estadoProyecto = estadoProyecto;
    }

    public List<SelectItem> getEmpresasOptions() {
        return empresasOptions;
    }

    public void setEmpresasOptions(List<SelectItem> empresasOptions) {
        this.empresasOptions = empresasOptions;
    }

    public List<SelectItem> getEstadosOptions() {
        return estadosOptions;
    }

    public void setEstadosOptions(List<SelectItem> estadosOptions) {
        this.estadosOptions = estadosOptions;
    }

    public PanelFiltroBusquedaProyectoBean getPanelFiltroBusquedaProyectoBean() {
        return panelFiltroBusquedaProyectoBean;
    }

    public void setPanelFiltroBusquedaProyectoBean(PanelFiltroBusquedaProyectoBean panelFiltroBusquedaProyectoBean) {
        this.panelFiltroBusquedaProyectoBean = panelFiltroBusquedaProyectoBean;
    }

}
