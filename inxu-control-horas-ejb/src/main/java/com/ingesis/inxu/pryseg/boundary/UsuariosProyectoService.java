package com.ingesis.inxu.pryseg.boundary;

import com.ingesis.inxu.pryseg.api.entity.PryUsuarioEntity;
import com.ingesis.inxu.pryseg.control.UsuariosProyecto;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import java.util.List;


@Stateless(name = "usuariosProyectoService")
@LocalBean
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class UsuariosProyectoService {

    @Inject
    private UsuariosProyecto usuariosProyecto;

    public List<PryUsuarioEntity> recuperarTodosLosUsuarios(){
        return usuariosProyecto.recuperarTodosLosUsuarios();
    }

    public Object recuperarUsuarioPorReferencia(Long ideUsuario){
        return usuariosProyecto.recuperarUsuarioPorReferencia(ideUsuario);
    }
}
