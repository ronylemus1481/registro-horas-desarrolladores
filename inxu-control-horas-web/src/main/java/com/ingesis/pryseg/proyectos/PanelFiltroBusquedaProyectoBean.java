package com.ingesis.pryseg.proyectos;

import com.ingesis.inxu.pryseg.api.entity.PryProyectoEntity;
import com.ingesis.inxu.pryseg.boundary.ProyectoService;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;

@ManagedBean
@ViewScoped
public class PanelFiltroBusquedaProyectoBean implements Serializable {

    private List<PryProyectoEntity> proyectoEntityList;

    @EJB
    private ProyectoService proyectoService;

    @PostConstruct
    public void init(){
        llenarListaProyectos();
    }

    public void llenarListaProyectos(){
        proyectoEntityList = proyectoService.recuperarTodosLosProyectos();
    }

    public List<PryProyectoEntity> getProyectoEntityList() {
        return proyectoEntityList;
    }

    public void setProyectoEntityList(List<PryProyectoEntity> proyectoEntityList) {
        this.proyectoEntityList = proyectoEntityList;
    }
}
