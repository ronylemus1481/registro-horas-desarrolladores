package com.ingesis.inxu.pryseg.control;

import com.ingesis.inxu.pryseg.api.entity.PryUsuarioEntity;
import com.ingesis.inxu.pryseg.dao.UsuariosProyectoDao;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
public class UsuariosProyecto {

    @Inject
    private UsuariosProyectoDao usuariosProyectoDao;


    public List<PryUsuarioEntity> recuperarTodosLosUsuarios(){
        return usuariosProyectoDao.recuperarTodosLosUsuarios();
    }

    public Object recuperarUsuarioPorReferencia(Long ideUsuario){
        return usuariosProyectoDao.getReference(ideUsuario);
    }

}
