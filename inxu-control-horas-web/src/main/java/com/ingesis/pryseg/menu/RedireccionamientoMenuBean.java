package com.ingesis.pryseg.menu;

import com.ingesis.inxu.pryseg.util.FacesUtil;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.IOException;
import java.io.Serializable;

@ManagedBean
@ViewScoped
public class RedireccionamientoMenuBean implements Serializable {


    public final static String URL_ROOT_CONTEXT = "/inxu-control-horas-web";

    public void redirect(String URL){
        try {
            new FacesUtil().redirectPage(URL_ROOT_CONTEXT+URL);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
