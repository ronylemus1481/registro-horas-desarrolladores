package com.ingesis.inxu.pryseg.util;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: rgonzalez
 * Date: 4/16/13
 * Time: 4:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class FacesUtil {
    /**
     * En un contexto de Java Server Faces, devuelve el <i>HttpServletRequest</i>
     *
     * @return
     */
    public  HttpServletRequest getHttpServletRequest() {
        HttpServletRequest request;
        request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        return request;
    }

    /**
     * Agrega un mensaje de error al contexto de JSF.
     *
     * @param message
     */
    public  void addErrorMessage(String message) {
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
    }

    /**
     * Agrega un mensaje informativo al contexto de JSF.
     *
     * @param message
     */
    public  void addInfoMessage(String message) {
        FacesContext fc = FacesContext.getCurrentInstance();
        fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, null));
    }

    /**
     * Obtiene y devuelve un parametro asociado al <i>Request</i> dado el nombre del parámetro.
     * Devuelve nulo, si ese nombre de parámetro no existe.
     *
     * @param paramName
     * @return
     */
    public  String getRequestParameter(String paramName) {
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> paramMap = fc.getExternalContext().getRequestParameterMap();
        return paramMap.get(paramName);
    }

    public  Map<String, Object> getSessionMap() {
        FacesContext fc = FacesContext.getCurrentInstance();
        return fc.getExternalContext().getSessionMap();
    }

    public  void redirectPage(String nombrePagina) throws IOException {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        externalContext.redirect(nombrePagina);
    }

    public  String getContextPath() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        return externalContext.getRequestContextPath();
    }

    public  String getMessage(Exception e,int tamLimite){

        int tam = 0;
        String mensaje = "";

        if(e.getMessage()!= null && e.getMessage().length() > 0) {
            tam = e.getMessage().length();
            if (tam > tamLimite) {
                mensaje = e.getMessage().substring(0, (tamLimite - 1));
            } else {
                mensaje = e.getMessage().substring(0, (tam - 1));
            }
        }else{

            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));

            String mensajeReturn = errors.toString().toUpperCase();
            tam = mensajeReturn.length();
            if(tam > tamLimite){
                mensaje = e.getMessage().substring(0, (tamLimite - 1));
            }else{
                mensaje = e.getMessage().substring(0, (tam - 1));
            }
        }
        return mensaje;
    }

    public  String getException(Exception e, String textoBuscarIni, String textoBuscarFin) {
        String mensajeReturn = null;
        int found;
        StringWriter errors = new StringWriter();
        e.printStackTrace(new PrintWriter(errors));

        if (errors != null && errors.toString().length() > 0) {
            mensajeReturn = errors.toString().toUpperCase();
            found = mensajeReturn.indexOf(textoBuscarIni);

            if (found > 0) {
                if (textoBuscarFin == null || textoBuscarFin.trim().length() == 0) {
                    mensajeReturn = mensajeReturn.substring(found, mensajeReturn.lastIndexOf(textoBuscarIni));
                } else {
                    mensajeReturn = mensajeReturn.substring(found, mensajeReturn.indexOf(textoBuscarFin));
                }
            }

            System.out.println(mensajeReturn);
        }

        if (mensajeReturn == null) {
            return e.getMessage();
        }

        return mensajeReturn;
    }

    public  String getNavegador() {
        String browser = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getHeader("User-Agent");
        System.out.println("navegador: " + browser);
        return browser;
    }
    
    
}