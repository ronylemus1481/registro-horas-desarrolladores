package com.ingesis.inxu.pryseg.dao;

import com.ingesis.inxu.pryseg.api.entity.PryEmpresaEntity;
import javax.inject.Named;
import java.util.List;

@Named
public class EmpresaDAO extends AbstractPrySegDAO<PryEmpresaEntity,Long> {


    public List<PryEmpresaEntity> recuperarEmpresasPorEstado(String estado){
        QueryParameter queryParameter = QueryParameter.with("estadoEmpresa",estado);
        return findWithNamedQuery("PryEmpresaEntity.findAllByEstado",
                queryParameter.parameters());
    }

}
