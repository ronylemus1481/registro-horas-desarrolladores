package com.ingesis.inxu.pryseg.api.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

@NamedQueries({
        @NamedQuery(name="PryRegistroHorasEntity.findAllByFecha",
            query = "SELECT r FROM PryRegistroHorasEntity r "
                +" WHERE r.fechaDesa BETWEEN :fechaInicial AND :fechaFinal"
                +" ORDER BY r.fechaDesa DESC, r.pryUsuarioDesaByIdeUsuario.codUsuario ASC"),

        @NamedQuery(name="PryRegistroHorasEntity.findAllByFechaAndUsuario",
            query = "SELECT r FROM PryRegistroHorasEntity r "
                    +" WHERE r.fechaDesa BETWEEN :fechaInicial AND :fechaFinal"
                    +"   AND r.pryUsuarioDesaByIdeUsuario.codUsuario like :codUsuario"
                    +" ORDER BY r.pryUsuarioDesaByIdeUsuario.codUsuario ASC, r.fechaDesa DESC")
})

@Entity
@Table(name = "PRY_REGISTRO_HORAS", schema = "PRYSEG")
public class PryRegistroHorasEntity implements Serializable {

    private Long ideRegistroHoras;
    private BigDecimal horasDesa;
    private BigDecimal horasDesaExtra;
    private Date fechaDesa;
    private Date fechaRegistro;
    private String tituloDefecto;
    private String descripcionDefecto;
    private String solucionDefecto;
    private PryUsuarioEntity pryUsuarioDesaByIdeUsuario;
    private PryUsuarioEntity pryUsuarioRegistroByIdeUsuario;
    private PryTareasXProyectoEntity pryTareasXProyectoEntity;

    @Id
    @Column(name = "IDE_REGISTRO_HORAS")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "genIdeRegistroHoras")
    @TableGenerator(name = "genIdeRegistroHoras", table = "pry_secuencia_tablas",
            pkColumnName = "cod_secuencia", pkColumnValue = "pry_registro_horas",
            valueColumnName = "correlativo", initialValue = 1, allocationSize = 1)
    public Long getIdeRegistroHoras() {
        return ideRegistroHoras;
    }

    public void setIdeRegistroHoras(Long ideRegistroHoras) {
        this.ideRegistroHoras = ideRegistroHoras;
    }

    @Basic
    @Column(name = "HORAS_DESA")
    public BigDecimal getHorasDesa() {
        return horasDesa;
    }

    public void setHorasDesa(BigDecimal horasDesa) {
        this.horasDesa = horasDesa;
    }

    @Basic
    @Column(name = "HORAS_DESA_EXTRA")
    public BigDecimal getHorasDesaExtra() {
        return horasDesaExtra;
    }

    public void setHorasDesaExtra(BigDecimal horasDesaExtra) {
        this.horasDesaExtra = horasDesaExtra;
    }

    @Basic
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_DESA")
    public Date getFechaDesa() {
        return fechaDesa;
    }

    public void setFechaDesa(Date fechaDesa) {
        this.fechaDesa = fechaDesa;
    }

    @Basic
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FECHA_REGISTRO")
    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Basic
    @Column(name = "TITULO_DEFECTO")
    public String getTituloDefecto() {
        return tituloDefecto;
    }

    public void setTituloDefecto(String tituloDefecto) {
        this.tituloDefecto = tituloDefecto;
    }

    @Basic
    @Column(name = "DESCRIPCION_DEFECTO")
    public String getDescripcionDefecto() {
        return descripcionDefecto;
    }

    public void setDescripcionDefecto(String descripcionDefecto) {
        this.descripcionDefecto = descripcionDefecto;
    }

    @Basic
    @Column(name = "SOLUCION_DEFECTO")
    public String getSolucionDefecto() {
        return solucionDefecto;
    }

    public void setSolucionDefecto(String solucionDefecto) {
        this.solucionDefecto = solucionDefecto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PryRegistroHorasEntity that = (PryRegistroHorasEntity) o;
        return Objects.equals(ideRegistroHoras, that.ideRegistroHoras) && Objects.equals(horasDesa, that.horasDesa) &&
                Objects.equals(fechaDesa, that.fechaDesa) && Objects.equals(fechaRegistro, that.fechaRegistro) &&
                Objects.equals(horasDesaExtra,that.horasDesaExtra) && Objects.equals(tituloDefecto,that.tituloDefecto) &&
                Objects.equals(descripcionDefecto,that.descripcionDefecto) && Objects.equals(solucionDefecto,that.solucionDefecto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ideRegistroHoras, horasDesa, fechaDesa, fechaRegistro,horasDesaExtra,tituloDefecto,
                descripcionDefecto,solucionDefecto);
    }

    @ManyToOne
    @JoinColumn(name = "IDE_USUARIO", referencedColumnName = "IDE_USUARIO")
    public PryUsuarioEntity getPryUsuarioDesaByIdeUsuario() {
        return pryUsuarioDesaByIdeUsuario;
    }

    public void setPryUsuarioDesaByIdeUsuario(PryUsuarioEntity pryUsuarioDesaByIdeUsuario) {
        this.pryUsuarioDesaByIdeUsuario = pryUsuarioDesaByIdeUsuario;
    }

    @ManyToOne(optional = false)
    @JoinColumn(name = "IDE_USUARIO_REGISTRO", referencedColumnName = "IDE_USUARIO")
    public PryUsuarioEntity getPryUsuarioRegistroByIdeUsuario() {
        return pryUsuarioRegistroByIdeUsuario;
    }

    public void setPryUsuarioRegistroByIdeUsuario(PryUsuarioEntity pryUsuarioRegistroByIdeUsuario) {
        this.pryUsuarioRegistroByIdeUsuario = pryUsuarioRegistroByIdeUsuario;
    }

    @ManyToOne
    @JoinColumn(name = "IDE_TAREA_PRY", referencedColumnName = "IDE_TAREA_PRY")
    public PryTareasXProyectoEntity getPryTareasXProyectoEntity() {
        return pryTareasXProyectoEntity;
    }

    public void setPryTareasXProyectoEntity(PryTareasXProyectoEntity pryTareasXProyectoEntity) {
        this.pryTareasXProyectoEntity = pryTareasXProyectoEntity;
    }
}
