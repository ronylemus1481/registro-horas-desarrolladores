package com.ingesis.inxu.pryseg.dao;

import java.util.List;
import java.util.Map;

public interface GenericDAO<E, K> {

    E findById(K id);

    E getReference(K id);

    void create(E entity);

    void update(E entity);

    void remove(E entity);

    List findWithNamedQuery(String namedQueryName);

    Object findSingleResultWithNamedQuery(String namedQueryName);

    List findWithNamedQuery(String namedQueryName, Map<String, Object> parameters);

    Object findSingleResultWithNamedQuery(String namedQueryName, Map<String, Object> parameters);

    List findWithNamedQuery(String namedQueryName, Map<String, Object> parameters, int resultLimit);

}
