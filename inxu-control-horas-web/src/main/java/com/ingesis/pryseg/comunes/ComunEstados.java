package com.ingesis.pryseg.comunes;

import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.List;

public class ComunEstados {



    public List<SelectItem> listaDeEstados(){
        List<SelectItem> listaDeEstados = new ArrayList<>();

        listaDeEstados.add(new SelectItem("ACTIVO","ACTIVO"));
        listaDeEstados.add(new SelectItem("INACTIVO","INACTIVO"));

        return listaDeEstados;
    }

}
