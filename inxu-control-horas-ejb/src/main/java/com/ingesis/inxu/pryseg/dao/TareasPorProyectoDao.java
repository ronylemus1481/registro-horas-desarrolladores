package com.ingesis.inxu.pryseg.dao;

import com.ingesis.inxu.pryseg.api.entity.PryProyectoEntity;
import com.ingesis.inxu.pryseg.api.entity.PryTareasXProyectoEntity;
import java.util.List;

public class TareasPorProyectoDao extends AbstractPrySegDAO<PryTareasXProyectoEntity,Long>{

    public List<PryTareasXProyectoEntity> recuperarTareasPorProyecto(PryProyectoEntity proyectoEntity){
        QueryParameter queryParameter = QueryParameter.with("ideProyecto",proyectoEntity);
        return findWithNamedQuery("PryTareasXProyectoEntity.findAllByIdeProyecto",
                queryParameter.parameters());
    }

    public List<PryTareasXProyectoEntity> recuperarTareasPorEmpresa(Long ideEmpresa){
        QueryParameter queryParameter = QueryParameter.with("ideEmpresa",ideEmpresa);
        return findWithNamedQuery("PryTareasXProyectoEntity.findAllByIdeEmpresa",
                queryParameter.parameters());
    }

    public List<PryTareasXProyectoEntity> recuperarTodasLasTareas(){
        return findWithNamedQuery("PryTareasXProyectoEntity.findAll");
    }
}
