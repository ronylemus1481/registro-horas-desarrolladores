package com.ingesis.pryseg.empresas;

import com.ingesis.inxu.pryseg.api.entity.PryEmpresaEntity;
import com.ingesis.inxu.pryseg.boundary.EmpresaService;
import com.ingesis.pryseg.comunes.ComunEstados;


import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import java.util.List;

@ManagedBean
@ViewScoped
public class PanelRegistrarEmpresasBean {

    private Long ideEmpresa;
    private String codigoEmpresa;
    private String nombreEmpresa;
    private String estadoEmpresa;
    private List<SelectItem> listaDeEstadosParaEmpresas;

    @EJB
    private EmpresaService empresaService;

    @PostConstruct
    public void inicializarPanelEmpresa(){
        this.llenarListaDeEstadosParaEmpresa();
    }

    public void btnGrabarEmpesa(){
        this.grabarEmpresa(this.asignarValoresParaGrabarEmpresa());
    }

    private PryEmpresaEntity asignarValoresParaGrabarEmpresa(){
        PryEmpresaEntity pryEmpresaEntity = new PryEmpresaEntity();

        if (this.ideEmpresa == null) {
            pryEmpresaEntity.setIdeEmpresa(this.ideEmpresa);
        }

        pryEmpresaEntity.setCodigoEmpresa(this.codigoEmpresa);
        pryEmpresaEntity.setNombreEmpresa(this.nombreEmpresa);
        pryEmpresaEntity.setEstadoEmpresa(this.estadoEmpresa);

        return pryEmpresaEntity;
    }

    private void grabarEmpresa(PryEmpresaEntity pryEmpresaEntity){
        if (pryEmpresaEntity.getIdeEmpresa() == null) {
            empresaService.crearEmpresa(pryEmpresaEntity);
        }else {
            empresaService.modificarEmpresa(pryEmpresaEntity);
        }

        this.limpiarPanelDeEmpresa();
    }

    public void btnLimpiarEmpresa(){
        this.limpiarPanelDeEmpresa();
    }

    private void limpiarPanelDeEmpresa(){
        ideEmpresa = null;
        codigoEmpresa = null;
        nombreEmpresa = null;
        estadoEmpresa = null;
    }

    private void llenarListaDeEstadosParaEmpresa(){
        listaDeEstadosParaEmpresas = new ComunEstados().listaDeEstados();
    }

    public Long getIdeEmpresa() {
        return ideEmpresa;
    }

    public void setIdeEmpresa(Long ideEmpresa) {
        this.ideEmpresa = ideEmpresa;
    }

    public String getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(String codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getEstadoEmpresa() {
        return estadoEmpresa;
    }

    public void setEstadoEmpresa(String estadoEmpresa) {
        this.estadoEmpresa = estadoEmpresa;
    }

    public List<SelectItem> getListaDeEstadosParaEmpresas() {
        return listaDeEstadosParaEmpresas;
    }

    public void setListaDeEstadosParaEmpresas(List<SelectItem> listaDeEstadosParaEmpresas) {
        this.listaDeEstadosParaEmpresas = listaDeEstadosParaEmpresas;
    }
}
