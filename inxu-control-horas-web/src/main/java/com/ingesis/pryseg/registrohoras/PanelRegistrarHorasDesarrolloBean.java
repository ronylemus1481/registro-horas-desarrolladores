package com.ingesis.pryseg.registrohoras;

import com.ingesis.inxu.pryseg.api.entity.*;
import com.ingesis.inxu.pryseg.boundary.*;
import com.ingesis.inxu.pryseg.control.EstadosConstantes;
import com.ingesis.inxu.pryseg.util.DateUtil;
import com.ingesis.inxu.pryseg.util.FacesUtil;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@ManagedBean
@ViewScoped
public class PanelRegistrarHorasDesarrolloBean implements Serializable {

    private Long ideUsuario;
    private Long ideEmpresa;
    private Long ideProyecto;
    private Long ideTareaPry;
    private Long ideRegistroHoras;

    private String tituloDefecto;
    private String descripcionDefecto;
    private String solucionDefecto;

    private BigDecimal horasTrabajadas;
    private BigDecimal horasExtrasTrabajadas;

    private Date fechaDesarrollo = new DateUtil().addDays(new DateUtil().getFechaActual(),-1);

    private List<SelectItem> usuariosOptions;
    private List<SelectItem> empresasOptions;
    private List<SelectItem> proyectosOptions;
    private List<SelectItem> tareasOptions;


    @EJB
    private EmpresaService empresaService;
    @EJB
    private ProyectoService proyectoService;
    @EJB
    private UsuariosProyectoService usuariosProyectoService;
    @EJB
    private TareasPorProyectoService tareasPorProyectoService;
    @EJB
    private RegistroHorasService registroHorasService;

    @ManagedProperty(value = "#{panelFiltroBusquedaBean}")
    private PanelFiltroBusquedaBean panelFiltroBusquedaBean;

    @PostConstruct
    public void init(){
        llenarListaUsuarios();
        llenarListaEmpresas();

        System.out.println("Post Constructor PanelRegistrarHorasDesarrolloBean");
    }

    public void llenarListaEmpresas() {
        List<PryEmpresaEntity> list = empresaService.recuperarEmpresasPorEstado(EstadosConstantes.ACTIVO);
        empresasOptions = new ArrayList<>();

        for (PryEmpresaEntity empresaEntity : list){
            empresasOptions.add(new SelectItem(empresaEntity.getIdeEmpresa(),empresaEntity.getCodigoEmpresa()
                    + "-"+empresaEntity.getNombreEmpresa()));
        }
    }

    public void llenarListaUsuarios() {
        List<PryUsuarioEntity> list = usuariosProyectoService.recuperarTodosLosUsuarios();
        usuariosOptions = new ArrayList<>();
        for (PryUsuarioEntity usuarioEntity : list) {
            usuariosOptions.add(new SelectItem(usuarioEntity.getIdeUsuario(), usuarioEntity.getCodUsuario()));
        }
    }

    public void llenarListaProyectosPorEmpresa(){
        PryEmpresaEntity empresaEntity = (PryEmpresaEntity) empresaService.recuperarEmpresaPorReferencia(ideEmpresa);
        List<PryProyectoEntity> list = proyectoService.recuperarProyectosPorEmpresa(empresaEntity);
        proyectosOptions = new ArrayList<>();
        for (PryProyectoEntity proyectoEntity : list){
            proyectosOptions.add(new SelectItem(proyectoEntity.getIdeProyecto(),proyectoEntity.getNombreProyecto()));
        }
    }

    public void llenarListaTareasPorProyecto(){
        PryProyectoEntity proyectoEntity = (PryProyectoEntity) proyectoService.recuperarProyectoByReference(ideProyecto);
        List<PryTareasXProyectoEntity> list = tareasPorProyectoService.recuperarTareasPorProyecto(proyectoEntity);
        tareasOptions = new ArrayList<>();
        for (PryTareasXProyectoEntity tareasXProyectoEntity : list){
            tareasOptions.add(new SelectItem(tareasXProyectoEntity.getIdeTareaPry(),tareasXProyectoEntity.getDescripTarea()));
        }
    }

    public void agregarRegistroHora(){
        imprimirAsignacionVariables();

        PryUsuarioEntity usuarioEntityDesarrollo = (PryUsuarioEntity) usuariosProyectoService
                .recuperarUsuarioPorReferencia(ideUsuario);

        PryTareasXProyectoEntity tareasXProyectoEntity = (PryTareasXProyectoEntity) tareasPorProyectoService
                .recuperarTareaPorProyectoByReference(ideTareaPry);

        PryRegistroHorasEntity registroHorasEntity = new PryRegistroHorasEntity();

        registroHorasEntity.setPryUsuarioDesaByIdeUsuario(usuarioEntityDesarrollo);
        registroHorasEntity.setPryTareasXProyectoEntity(tareasXProyectoEntity);
        registroHorasEntity.setFechaDesa(fechaDesarrollo);
        registroHorasEntity.setHorasDesa(horasTrabajadas);
        registroHorasEntity.setHorasDesaExtra((horasExtrasTrabajadas == null ? BigDecimal.ZERO : horasExtrasTrabajadas));
        registroHorasEntity.setFechaRegistro(new DateUtil().getFechaActual());
        registroHorasEntity.setTituloDefecto(tituloDefecto);
        registroHorasEntity.setDescripcionDefecto(descripcionDefecto);
        registroHorasEntity.setSolucionDefecto(solucionDefecto);

        if (this.ideRegistroHoras != null){
            registroHorasEntity.setIdeRegistroHoras(ideRegistroHoras);
        }

        grabarRegistroHoras(registroHorasEntity);
        new FacesUtil().addInfoMessage("Se agregó el registro de horas satisfactoriamente.");
    }

    private void imprimirAsignacionVariables(){
        System.out.println("Agregando Usuario: "+ideUsuario);
        System.out.println("Agregando Empresa: "+ideEmpresa);
        System.out.println("Agregando Proyecto: "+ideProyecto);
        System.out.println("Agregando Tarea: "+ideTareaPry);
        System.out.println("Horas trabajadas: "+horasTrabajadas);
        System.out.println("Horas extras trabajadas: "+horasExtrasTrabajadas);
        System.out.println("Fecha desarrollo: "+fechaDesarrollo);
    }

    private void grabarRegistroHoras(PryRegistroHorasEntity registroHorasEntity){
        if (registroHorasEntity.getIdeRegistroHoras() == null) {
            registroHorasService.crearRegistroHoras(registroHorasEntity);
        }else {
            registroHorasService.modificarRegistroHoras(registroHorasEntity);
            inicializaRegistrarHorasDesarrollo();
        }

        panelFiltroBusquedaBean.llenarListaRegistroHoras();
    }

    public void asignarRegistroHorasParaModificar(PryRegistroHorasEntity registroHorasEntity){
        this.ideUsuario = registroHorasEntity.getPryUsuarioDesaByIdeUsuario().getIdeUsuario();
        this.ideEmpresa = registroHorasEntity.getPryTareasXProyectoEntity()
                .getPryProyectoByIdeProyecto().getPryEmpresaByIdeEmpresa().getIdeEmpresa();
        this.ideProyecto = registroHorasEntity.getPryTareasXProyectoEntity()
                .getPryProyectoByIdeProyecto().getIdeProyecto();
        this.ideTareaPry = registroHorasEntity.getPryTareasXProyectoEntity().getIdeTareaPry();
        this.ideRegistroHoras = registroHorasEntity.getIdeRegistroHoras();
        this.horasTrabajadas = registroHorasEntity.getHorasDesa();
        this.horasExtrasTrabajadas = registroHorasEntity.getHorasDesaExtra();
        this.fechaDesarrollo = registroHorasEntity.getFechaDesa();
        this.tituloDefecto = registroHorasEntity.getTituloDefecto();
        this.descripcionDefecto = registroHorasEntity.getDescripcionDefecto();
        this.solucionDefecto = registroHorasEntity.getSolucionDefecto();

        llenarListaProyectosPorEmpresa();
        llenarListaTareasPorProyecto();
    }

    public void eliminarRegistroHora(PryRegistroHorasEntity registroHorasEntity){
        registroHorasService.eliminarRegistroHoras(registroHorasEntity);
        panelFiltroBusquedaBean.llenarListaRegistroHoras();
        new FacesUtil().addInfoMessage("Se ha eliminado el registro.");
    }

    public void inicializaRegistrarHorasDesarrollo(){
        this.ideUsuario = null;
        this.ideEmpresa = null;
        this.ideProyecto = null;
        this.ideTareaPry = null;
        this.ideRegistroHoras = null;
        this.horasTrabajadas = null;
        this.horasExtrasTrabajadas = null;
        this.fechaDesarrollo = new DateUtil().addDays(new DateUtil().getFechaActual(),-1);
        this.proyectosOptions = null;
        this.tareasOptions = null;
        this.tituloDefecto = null;
        this.descripcionDefecto = null;
        this.solucionDefecto = null;
        llenarListaUsuarios();
        llenarListaEmpresas();
    }

    public List<SelectItem> getUsuariosOptions() {
        return usuariosOptions;
    }

    public void setUsuariosOptions(List<SelectItem> usuariosOptions) {
        this.usuariosOptions = usuariosOptions;
    }

    public List<SelectItem> getEmpresasOptions() {
        return empresasOptions;
    }

    public void setEmpresasOptions(List<SelectItem> empresasOptions) {
        this.empresasOptions = empresasOptions;
    }

    public List<SelectItem> getProyectosOptions() {
        return proyectosOptions;
    }

    public void setProyectosOptions(List<SelectItem> proyectosOptions) {
        this.proyectosOptions = proyectosOptions;
    }

    public List<SelectItem> getTareasOptions() {
        return tareasOptions;
    }

    public void setTareasOptions(List<SelectItem> tareasOptions) {
        this.tareasOptions = tareasOptions;
    }

    public Long getIdeUsuario() {
        return ideUsuario;
    }

    public void setIdeUsuario(Long ideUsuario) {
        this.ideUsuario = ideUsuario;
    }

    public Long getIdeEmpresa() {
        return ideEmpresa;
    }

    public void setIdeEmpresa(Long ideEmpresa) {
        this.ideEmpresa = ideEmpresa;
    }

    public Long getIdeProyecto() {
        return ideProyecto;
    }

    public void setIdeProyecto(Long ideProyecto) {
        this.ideProyecto = ideProyecto;
    }

    public Long getIdeTareaPry() {
        return ideTareaPry;
    }

    public void setIdeTareaPry(Long ideTareaPry) {
        this.ideTareaPry = ideTareaPry;
    }

    public BigDecimal getHorasTrabajadas() {
        return horasTrabajadas;
    }

    public void setHorasTrabajadas(BigDecimal horasTrabajadas) {
        this.horasTrabajadas = horasTrabajadas;
    }

    public BigDecimal getHorasExtrasTrabajadas() {
        return horasExtrasTrabajadas;
    }

    public void setHorasExtrasTrabajadas(BigDecimal horasExtrasTrabajadas) {
        this.horasExtrasTrabajadas = horasExtrasTrabajadas;
    }

    public Date getFechaDesarrollo() {
        return fechaDesarrollo;
    }

    public void setFechaDesarrollo(Date fechaDesarrollo) {
        this.fechaDesarrollo = fechaDesarrollo;
    }

    public PanelFiltroBusquedaBean getPanelFiltroBusquedaBean() {
        return panelFiltroBusquedaBean;
    }

    public void setPanelFiltroBusquedaBean(PanelFiltroBusquedaBean panelFiltroBusquedaBean) {
        this.panelFiltroBusquedaBean = panelFiltroBusquedaBean;
    }

    public String getTituloDefecto() {
        return tituloDefecto;
    }

    public void setTituloDefecto(String tituloDefecto) {
        this.tituloDefecto = tituloDefecto;
    }

    public String getDescripcionDefecto() {
        return descripcionDefecto;
    }

    public void setDescripcionDefecto(String descripcionDefecto) {
        this.descripcionDefecto = descripcionDefecto;
    }

    public String getSolucionDefecto() {
        return solucionDefecto;
    }

    public void setSolucionDefecto(String solucionDefecto) {
        this.solucionDefecto = solucionDefecto;
    }
}
