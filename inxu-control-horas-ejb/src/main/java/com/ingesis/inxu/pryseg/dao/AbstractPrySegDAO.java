package com.ingesis.inxu.pryseg.dao;

/**
 * Created by renatogonzalez on 5/10/17.
 */

import com.ingesis.inxu.pryseg.dao.GenericDAO;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;
import java.util.Set;


public abstract class AbstractPrySegDAO<E, K> implements GenericDAO<E, K> {

    @PersistenceContext(unitName = "inxu-pryseguros-pu")
    protected EntityManager entityManager;
    protected Class entityClass;

    public AbstractPrySegDAO() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class<E>) genericSuperclass.getActualTypeArguments()[0];
    }

    @Override
    public void create(E entity) {
        entityManager.persist(entity);
    }

    @Override
    public E findById(K id) {
        return (E) entityManager.find(entityClass, id);
    }

    @Override
    public E getReference(K id) {
        return (E) entityManager.getReference(entityClass, id);
    }

    @Override
    public void remove(E entity) {
        E mergedEntity = null;
        if (entityManager.contains(entity)) {
            mergedEntity = entity;
        } else {
            mergedEntity = entityManager.merge(entity);
        }
        entityManager.remove(mergedEntity);
    }

    @Override
    public void update(E entity) {
        E mergedEntity = entityManager.merge(entity);
        entityManager.persist(mergedEntity);
    }

    @Override
    public List findWithNamedQuery(String namedQueryName) {
        return entityManager.createNamedQuery(namedQueryName).getResultList();
    }

    @Override
    public Object findSingleResultWithNamedQuery(String namedQueryName) {
        List resultList = findWithNamedQuery(namedQueryName);
        return getCollectionFirstElement(resultList);
    }

    @Override
    public List findWithNamedQuery(String namedQueryName, Map<String, Object> parameters) {
        return findWithNamedQuery(namedQueryName, parameters, 0);
    }

    @Override
    public Object findSingleResultWithNamedQuery(String namedQueryName, Map<String, Object> parameters) {
        List resultList = findWithNamedQuery(namedQueryName, parameters);
        return getCollectionFirstElement(resultList);
    }

    @Override
    public List findWithNamedQuery(String namedQueryName, Map<String, Object> parameters, int resultLimit) {
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = entityManager.createNamedQuery(namedQueryName);
        if (resultLimit > 0) {
            query.setMaxResults(resultLimit);
        }
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        return query.getResultList();
    }


    private Object getCollectionFirstElement(List list) {
        Object object = null;
        if (list != null && list.size() > 0) {
            object = list.get(0);
        }
        return object;
    }


}

