package com.ingesis.pryseg.registrohoras;


import com.ingesis.inxu.pryseg.api.entity.PryRegistroHorasEntity;
import com.ingesis.inxu.pryseg.api.entity.PryUsuarioEntity;
import com.ingesis.inxu.pryseg.boundary.RegistroHorasService;
import com.ingesis.inxu.pryseg.util.DateUtil;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

@ManagedBean
@ViewScoped
public class PanelFiltroBusquedaBean implements Serializable {

    private String codigoUsuario;
    private Date fechaInicial;
    private Date fechaFinal;

    private List<PryRegistroHorasEntity> registroHorasEntityList;

    @EJB
    private RegistroHorasService registroHorasService;

    @PostConstruct
    public void inti(){
        this.fechaInicial = new DateUtil().truncate(new DateUtil().obtienePrimerDiaSemana(new DateUtil().getFechaActual()));
        this.fechaFinal = new DateUtil().addDays(new DateUtil().truncate(new DateUtil().getFechaActual()),1);
        aplicarFiltro();
    }

    public void llenarListaRegistroHoras(){
        if (codigoUsuario != null && codigoUsuario.trim().length()> 0){
            PryUsuarioEntity usuarioEntity = new PryUsuarioEntity();
            usuarioEntity.setCodUsuario(this.codigoUsuario);
            registroHorasEntityList = registroHorasService.recuperarRegistroHorasPorUsuarioFecha(fechaInicial,fechaFinal,
                    usuarioEntity);
        }else {
            registroHorasEntityList = registroHorasService.recuperarRegistroHorasPorFecha(this.fechaInicial, this.fechaFinal);
        }
    }

    public void aplicarFiltro(){
        llenarListaRegistroHoras();
    }

    public TimeZone getTimeZone() {
        DateUtil dateUtil = new DateUtil();
        return dateUtil.getTimeZone();
    }

    public String getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(String codigoUsuario) {
        this.codigoUsuario = (codigoUsuario != null ? codigoUsuario.trim().toLowerCase() : null);
    }

    public Date getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(Date fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public List<PryRegistroHorasEntity> getRegistroHorasEntityList() {
        return registroHorasEntityList;
    }

    public void setRegistroHorasEntityList(List<PryRegistroHorasEntity> registroHorasEntityList) {
        this.registroHorasEntityList = registroHorasEntityList;
    }

}
