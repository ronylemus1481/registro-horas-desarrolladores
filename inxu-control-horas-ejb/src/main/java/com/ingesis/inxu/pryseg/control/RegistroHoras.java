package com.ingesis.inxu.pryseg.control;

import com.ingesis.inxu.pryseg.api.entity.PryRegistroHorasEntity;
import com.ingesis.inxu.pryseg.api.entity.PryUsuarioEntity;
import com.ingesis.inxu.pryseg.control.QueryParameter;
import com.ingesis.inxu.pryseg.dao.RegistroHorasDao;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.List;

@Named
public class RegistroHoras {


    @Inject
    private RegistroHorasDao registroHorasDao;





    public void crearRegistroHoras(PryRegistroHorasEntity registroHorasEntity){
        registroHorasDao.create(registroHorasEntity);
    }

    public void eliminarRegistroHoras(PryRegistroHorasEntity registroHorasEntity){
        registroHorasDao.remove(registroHorasEntity);
    }

    public void modificarRegistroHoras(PryRegistroHorasEntity registroHorasEntity){
        registroHorasDao.update(registroHorasEntity);
    }

    public List<PryRegistroHorasEntity> recuperarRegistroHorasPorFecha(Date fechaInicial, Date fechaFinal){
        QueryParameter queryParameter = QueryParameter.with("fechaInicial",fechaInicial).and("fechaFinal",
                fechaFinal);

        return registroHorasDao.findWithNamedQuery("PryRegistroHorasEntity.findAllByFecha",
                queryParameter.parameters());
    }

    public List<PryRegistroHorasEntity> recuperarRegistroHorasPorUsuarioFecha(Date fechaInicial, Date fechaFinal,
                                                                              PryUsuarioEntity usuarioEntity){
        QueryParameter queryParameter = QueryParameter.with("fechaInicial",fechaInicial)
                .and("fechaFinal",fechaFinal).and("codUsuario",usuarioEntity.getCodUsuario()+"%");

        return registroHorasDao.findWithNamedQuery("PryRegistroHorasEntity.findAllByFechaAndUsuario",
                queryParameter.parameters());
    }
}
