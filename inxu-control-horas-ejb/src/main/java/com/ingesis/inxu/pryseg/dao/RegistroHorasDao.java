package com.ingesis.inxu.pryseg.dao;

import com.ingesis.inxu.pryseg.api.entity.PryRegistroHorasEntity;

import javax.inject.Named;

@Named
public class RegistroHorasDao extends AbstractPrySegDAO<PryRegistroHorasEntity, Long> {
}
