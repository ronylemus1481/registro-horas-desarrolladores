package com.ingesis.inxu.pryseg.util;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Created with IntelliJ IDEA.
 * User: rony
 * Date: 4/25/13
 * Time: 10:00 AM
 * To change this template use File | Settings | File Templates.
 */
public class DateUtil {
    public final static long DAY_MILISECONDS = 86400000;
    public final static long YEAR_MILISECONDS = 31536000000l;
    public final static String FORMATO_DDMMMYYYY_SLASH = "dd/MM/yyyy";
    public final static String FORMATO_yyyyMMdd = "yyyyMMdd";

    /**
     * 0 NADA
     * 1 DOMINGO
     * ..
     * 7 SABADO
     */
    public final String DIA_SEMANA[] =
            {
                    "",
                    "Domingo",
                    "Lunes",
                    "Martes",
                    "Miercoles",
                    "Jueves",
                    "Viernes",
                    "Sabado"
            };


    /**
     * constructor *
     */
    public DateUtil() {

    }

    public long diferenciaEnDiasZonaHoraria(Date fecha, Date hoy) {
        TimeZone timeZone1 = TimeZone.getTimeZone("America/El_Salvador");
        long res = 0;
        Calendar fecha1 = Calendar.getInstance(timeZone1);
        Calendar hoy1 = Calendar.getInstance(timeZone1);
        fecha1.setTime(truncate(fecha));
        hoy1.setTime(truncate(hoy));
        long ultimafecha=hoy1.getTimeInMillis()/ DAY_MILISECONDS;
        long inicioFecha=fecha1.getTimeInMillis()/DAY_MILISECONDS;
        //res = (hoy1.getTimeInMillis() - fecha1.getTimeInMillis()) / DAY_MILISECONDS;
        res=ultimafecha-inicioFecha;
        return res;
    }

    public Date convertXmlGregoriaCalendarToDate(XMLGregorianCalendar xml){
        return xml.toGregorianCalendar().getTime();
    }

    /**
     * Método para calcular la diferencia entre dos fechas en semanas
     *
     * @param fecha fecha inicial
     * @param hoy   fecha final
     * @return devuelve el numero de semanas de hoy - fecha
     */
    public long diferenciEnSemanas(Date fecha, Date hoy) {
        long res = 0;
        Calendar fecha1 = Calendar.getInstance();
        Calendar hoy1 = Calendar.getInstance();
        fecha1.setTime(truncate(fecha));
        hoy1.setTime(truncate(hoy));
        int semanaHoy = 0;
        int semanaFecha = 0;

        if (hoy1.get(Calendar.YEAR) != fecha1.get(Calendar.YEAR)) {
            if (Math.abs((hoy1.getTimeInMillis() - fecha1.getTimeInMillis()) / DAY_MILISECONDS) >= 7) {
                res = (hoy1.getTimeInMillis() - fecha1.getTimeInMillis()) / (DAY_MILISECONDS * 7);
            } else if (fecha.compareTo(hoy) < 0)
                if (hoy1.get(Calendar.DAY_OF_WEEK) < fecha1.get(Calendar.DAY_OF_WEEK)) {
                    res = 1;
                } else
                    res = 0;
            else if (hoy1.get(Calendar.DAY_OF_WEEK) > fecha1.get(Calendar.DAY_OF_WEEK)) {
                res = -1;
            } else
                res = 0;
        } else {
            semanaHoy = hoy1.get(Calendar.WEEK_OF_YEAR);
            semanaFecha = fecha1.get(Calendar.WEEK_OF_YEAR);

            if (hoy1.get(Calendar.MONTH) == Calendar.DECEMBER && (semanaHoy == 1)) {
                semanaHoy = 53;
            }
            if (fecha1.get(Calendar.MONTH) == Calendar.DECEMBER && (semanaFecha == 1)) {
                semanaFecha = 53;
            }

            res = semanaHoy - semanaFecha;
        }
        return res;
    }


    /**
     * Método que devuelve el primer día del mes
     *
     * @return
     */
    public Date getPrimerDiaDelMes() {
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.getActualMinimum(Calendar.DAY_OF_MONTH),
                cal.getMinimum(Calendar.HOUR_OF_DAY),
                cal.getMinimum(Calendar.MINUTE),
                cal.getMinimum(Calendar.SECOND));

        return cal.getTime();
    }

    /**
     * Método que devuelve el último día del mes
     *
     * @return
     */
    public Date getUltimoDiaDelMesActual() {
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.getActualMaximum(Calendar.DAY_OF_MONTH),
                cal.getMaximum(Calendar.HOUR_OF_DAY),
                cal.getMaximum(Calendar.MINUTE),
                cal.getMaximum(Calendar.SECOND));
        return cal.getTime();
    }

    public Date getUltimoDiaDelMes(Date fecha) {
        Calendar cal = Calendar.getInstance();
        Calendar calendar = Calendar.getInstance();
        cal.setTime(fecha);

        calendar.set(cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.getActualMaximum(Calendar.DAY_OF_MONTH),
                cal.getMaximum(Calendar.HOUR_OF_DAY),
                cal.getMaximum(Calendar.MINUTE),
                cal.getMaximum(Calendar.SECOND));

        return calendar.getTime();
    }

    public Date getPrimerDiaDelMes(Date fecha) {
        Calendar cal = Calendar.getInstance();
        Calendar calendar = Calendar.getInstance();
        cal.setTime(fecha);

        calendar.set(cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.getActualMinimum(Calendar.DAY_OF_MONTH),
                cal.getMaximum(Calendar.HOUR_OF_DAY),
                cal.getMaximum(Calendar.MINUTE),
                cal.getMaximum(Calendar.SECOND));

        return calendar.getTime();
    }


    public Date getFechaActual() {
        Date fecha = new Date();
        return fecha;
    }

    /**
     * Método para quitar la hora de una fecha y dejarla en 00:00:00
     *
     * @param fecha fecha a truncar
     * @return fecha truncada con hora 00 minutos 00 segundos 00
     */
    public Date truncate(Date fecha) {
        Calendar calendar = Calendar.getInstance();
        Calendar calendar1 = Calendar.getInstance();
        calendar.setTime(fecha);
        calendar1.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        return calendar1.getTime();
    }

    /**
     * Método que calcula la diferencia en meses entre dos fechas
     *
     * @param fecha
     * @param hoy
     * @return
     */
    public long diferenciaEnMeses(Date fecha, Date hoy) {
        long res = 0;
        Calendar fecha1 = Calendar.getInstance();
        Calendar hoy1 = Calendar.getInstance();
        fecha1.setTime(truncate(fecha));
        hoy1.setTime(truncate(hoy));

        res = (hoy1.get(Calendar.YEAR) * 12 + hoy1.get(Calendar.MONTH) + 1) -
                (fecha1.get(Calendar.YEAR) * 12 + fecha1.get(Calendar.MONTH) + 1);


        return res;
    }

    public long diferenciaEnDias(Date fecha, Date hoy) {
        long res = 0;
        Calendar fecha1 = Calendar.getInstance();
        Calendar hoy1 = Calendar.getInstance();
        fecha1.setTime(truncate(fecha));
        hoy1.setTime(truncate(hoy));
        res = (hoy1.getTimeInMillis() - fecha1.getTimeInMillis()) / DAY_MILISECONDS;
        return res;
    }

    public long diferenciaEnMinutos(Date fecha, Date hoy) {
        long res = 0;
        Calendar fecha1 = Calendar.getInstance();
        Calendar hoy1 = Calendar.getInstance();
        fecha1.setTime(fecha);
        hoy1.setTime(hoy);
        res = (fecha1.getTimeInMillis() - hoy1.getTimeInMillis());
        System.out.println("Diferencia Misisegundos; " + res);
        res = (res / 1000 / 60);

        return res;
    }


    /**
     * Agrega o quita minutos a una fecha dada. Para quitar minutos hay que
     * sumarle valores negativos.
     *
     * @param date
     * @param minutes
     * @return
     */
    public Date addMinutes(Date date, int minutes) {
        Calendar calendarDate = Calendar.getInstance();
        calendarDate.setTime(date);
        calendarDate.add(Calendar.MINUTE, minutes);
        return calendarDate.getTime();
    }

    /**
     * Agrega o quita Meses a una fecha dada. Para quitar minutos hay que
     * sumarle valores negativos.
     *
     * @param date
     * @param months
     * @return
     */

    public Date addMonths(Date date, int months) {
        Calendar calendarDate = Calendar.getInstance();
        calendarDate.setTime(date);
        calendarDate.add(Calendar.MONTH, months);
        return calendarDate.getTime();
    }

    /**
     * @param date
     * @param days
     * @return
     */
    public Date addDays(Date date, int days) {
        return addMinutes(date, 60 * 24 * days);
    }

    public String convertToString(Date fecha, String formato) {
        SimpleDateFormat formatoFecha = new SimpleDateFormat(formato);
        String fechaString = formatoFecha.format(fecha);

        return fechaString;
    }

    public XMLGregorianCalendar convertToXMLGregorianCalendar(Date fecha) throws DatatypeConfigurationException {
        if( fecha == null )
            return null;
        GregorianCalendar gregory = new GregorianCalendar();
        gregory.setTime(fecha);
        XMLGregorianCalendar calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregory);
        return calendar;
    }

    public String descripcionAgrupacion(Date fecha, Date hoy) {
        long difSemanas;
        long difDias;
        long difMeses = 0;
        String resultado = "";
        difSemanas = diferenciEnSemanas(fecha, hoy);
        if (difSemanas == 0) {
            difDias = diferenciaEnDias(fecha, hoy);
            switch ((int) difDias) {
                case 0:
                    resultado = "Hoy";
                    break;
                case 1:
                    resultado = "Ayer";
                    break;
                case -1:
                    resultado = "Mañana";
                    break;
                default:
                    if (difDias > 1) {
                        resultado = "Día " + diaSemana(fecha);
                    }
                    if (difDias < -1) {
                        resultado = "Próximo " + diaSemana(fecha);
                    }
            }
        } else {
            if (Math.abs(difSemanas) <= 4) {
                switch ((int) difSemanas) {
                    case 1:
                        resultado = "Semana Pasada";
                        break;
                    case 2:
                        resultado = "Hace dos semanas";
                        break;
                    case 3:
                        resultado = "Hace tres semanas";
                        break;
                    case 4:
                        resultado = "Hace 1 mes";
                        break;
                    case -1:
                        resultado = "Próxima Semana";
                        break;
                    case -2:
                        resultado = "Dentro de dos semanas";
                        break;
                    case -3:
                        resultado = "Dentro de tres semanas";
                        break;
                }
            } else {
                difMeses = diferenciaEnMeses(fecha, hoy);
                if (Math.abs(difMeses) < 2) {
                    switch ((int) difMeses) {
                        case 1:
                            resultado = "Hace 1 mes";
                            break;
                        case -1:
                            resultado = "Próximo mes";
                            break;
                    }
                } else {
                    if (difMeses > 1) {
                        if (difMeses < 36) {
                            resultado = "Hace " + difMeses + " meses";
                        } else resultado = "Meses anteriores";
                    }
                    if (difMeses < -1) {
                        resultado = "Dentro de " + Math.abs(difMeses) + " meses";
                    }
                }
            }
        }
        return resultado;
    }


    /**
     * Método devuelve dia de la semana en español
     * Domingo,Lunes, Martes .. Sabado
     *
     * @param fecha
     * @return
     */
    public String diaSemana(Date fecha) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);

        return DIA_SEMANA[calendar.get(Calendar.DAY_OF_WEEK)];
    }


    public Date convertStringToDate(String fecha, String formato) {

        SimpleDateFormat formatoDelTexto = new SimpleDateFormat(formato);
        Date retFecha = null;

        try {
            retFecha = formatoDelTexto.parse(fecha);
        } catch (ParseException ex) {
            return retFecha;
        }

        return retFecha;
    }

    public TimeZone getTimeZone() {
        return TimeZone.getDefault();
    }

    public Date obtienePrimerDiaMes(Date fecha) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(fecha);
        cal.set(cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.getActualMinimum(Calendar.DAY_OF_MONTH),
                cal.getMinimum(Calendar.HOUR_OF_DAY),
                cal.getMinimum(Calendar.MINUTE),
                cal.getMinimum(Calendar.SECOND));
        return cal.getTime();
    }

    public Date obtieneUltimoDiaMes(Date fecha) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(fecha);
        cal.set(cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.getActualMaximum(Calendar.DAY_OF_MONTH),
                cal.getMaximum(Calendar.HOUR_OF_DAY),
                cal.getMaximum(Calendar.MINUTE),
                cal.getMaximum(Calendar.SECOND));
        return cal.getTime();
    }

    public boolean esBisiesto(Integer anio) {
        GregorianCalendar calendar = new GregorianCalendar();
        if (calendar.isLeapYear(anio)) {
            return true;
        } else {
            return false;
        }
    }

    //Retorna la fecha actual con un formato especifico
    public Date getFechaActualConFormato(String formato) {
        DateFormat formateador = new SimpleDateFormat(formato);
        return convertStringToDate(formateador.format(getFechaActual()), formato);
    }

    //Retorna la fecha actual con un formato especifico
    public Date getFechaConFormato(Date fecha, String formato) {
        DateFormat formateador = new SimpleDateFormat(formato);
        return convertStringToDate(formateador.format(fecha), formato);
    }


    public String convertDateToStringWithFormat(Date fecha, String formato) {
        DateFormat formateador = new SimpleDateFormat(formato);
        return formateador.format(fecha);
    }

    public String nombreDelMes(Date fecha) {
        String nombreMes = "";
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        int mes = 0;

        try {
            mes = calendar.get(Calendar.MONTH);
        } catch (Exception ex) {
        }
        switch (mes) {
            case 0: {
                nombreMes = "Enero";
                break;
            }
            case 1: {
                nombreMes = "Febrero";
                break;
            }
            case 2: {
                nombreMes = "Marzo";
                break;
            }
            case 3: {
                nombreMes = "Abril";
                break;
            }
            case 4: {
                nombreMes = "Mayo";
                break;
            }
            case 5: {
                nombreMes = "Junio";
                break;
            }
            case 6: {
                nombreMes = "Julio";
                break;
            }
            case 7: {
                nombreMes = "Agosto";
                break;
            }
            case 8: {
                nombreMes = "Septiembre";
                break;
            }
            case 9: {
                nombreMes = "Octubre";
                break;
            }
            case 10: {
                nombreMes = "Noviembre";
                break;
            }
            case 11: {
                nombreMes = "Diciembre";
                break;
            }
            default: {
                nombreMes = "Error";
                break;
            }
        }
        return nombreMes;
    }

    public String obtenerAnio(Date fecha) {
        String anio = "";
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        int mes = 0;

        try {
            mes = calendar.get(Calendar.YEAR);
            anio = String.valueOf(mes);
        } catch (Exception ex) {
        }

        return anio;
    }

    public String obtenerDia(Date fecha) {
        String anio = "";
        String dia = "";
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        int mes = 0;

        try {
            dia = Integer.toString(calendar.get(Calendar.DAY_OF_MONTH));
            mes = calendar.get(Calendar.YEAR);
            anio = String.valueOf(mes);
        } catch (Exception ex) {
        }

        return dia;
    }


    public long diferenciaEnAnios(Date fecha, Date hoy) {
        long res = 0;
        Calendar fecha1 = Calendar.getInstance();
        Calendar hoy1 = Calendar.getInstance();
        fecha1.setTime(truncate(fecha));
        hoy1.setTime(truncate(hoy));
        res = (hoy1.getTimeInMillis() - fecha1.getTimeInMillis()) / YEAR_MILISECONDS;
        return res;
    }

    public Date obtienePrimerDiaSemana(Date fechaCalculo){
        Calendar fecha1 = GregorianCalendar.getInstance();
        fecha1.setTime(fechaCalculo);

        System.out.println("Current week = " + Calendar.DAY_OF_WEEK);

        // Set the calendar to monday of the current week
        fecha1.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        System.out.println("Current week = " + Calendar.DAY_OF_WEEK);
        return fecha1.getTime();
    }

    public Date obtieneUltimoDiaSemana(Date fechaCalculo){
        Date primerDiaSemana = obtienePrimerDiaSemana(fechaCalculo);
        return addDays(primerDiaSemana,7);
    }
}
