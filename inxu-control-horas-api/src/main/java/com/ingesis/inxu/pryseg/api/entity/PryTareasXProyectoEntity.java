package com.ingesis.inxu.pryseg.api.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "PRY_TAREAS_X_PROYECTO", schema = "PRYSEG")
@NamedQueries({
        @NamedQuery(name="PryTareasXProyectoEntity.findAll",
                query = "SELECT r FROM PryTareasXProyectoEntity r " +
                        "ORDER BY r.pryProyectoByIdeProyecto.pryEmpresaByIdeEmpresa.nombreEmpresa, " +
                        " r.pryProyectoByIdeProyecto.nombreProyecto, r.descripTarea"),

        @NamedQuery(name="PryTareasXProyectoEntity.findAllByIdeProyecto",
                query = "SELECT r FROM PryTareasXProyectoEntity r WHERE r.pryProyectoByIdeProyecto = :ideProyecto " +
                        "ORDER BY r.pryProyectoByIdeProyecto.pryEmpresaByIdeEmpresa.nombreEmpresa, " +
                        " r.pryProyectoByIdeProyecto.nombreProyecto"),

        @NamedQuery(name="PryTareasXProyectoEntity.findAllByIdeEmpresa",
                query = "SELECT r FROM PryTareasXProyectoEntity r " +
                        "WHERE r.pryProyectoByIdeProyecto.pryEmpresaByIdeEmpresa.ideEmpresa = :ideEmpresa " +
                        "ORDER BY r.pryProyectoByIdeProyecto.nombreProyecto")

})
public class PryTareasXProyectoEntity implements Serializable {

    private Long ideTareaPry;
    private String codigoTarea;
    private BigDecimal horasEstimadas;
    private String estadoTarea;
    private String descripTarea;
    private PryProyectoEntity pryProyectoByIdeProyecto;

    @Id
    @Column(name = "IDE_TAREA_PRY")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "genIdeTarea")
    @TableGenerator(name = "genIdeTarea", table = "pry_secuencia_tablas",
            pkColumnName = "cod_secuencia", pkColumnValue = "pry_tareas_x_proyecto",
            valueColumnName = "correlativo", initialValue = 1, allocationSize = 1)
    public Long getIdeTareaPry() {
        return ideTareaPry;
    }

    public void setIdeTareaPry(Long ideTareaPry) {
        this.ideTareaPry = ideTareaPry;
    }

    @Basic
    @Column(name = "CODIGO_TAREA")
    public String getCodigoTarea() {
        return codigoTarea;
    }

    public void setCodigoTarea(String codigoTarea) {
        this.codigoTarea = codigoTarea;
    }

    @Basic
    @Column(name = "HORAS_ESTIMADAS")
    public BigDecimal getHorasEstimadas() {
        return horasEstimadas;
    }

    public void setHorasEstimadas(BigDecimal horasEstimadas) {
        this.horasEstimadas = horasEstimadas;
    }

    @Basic
    @Column(name = "ESTADO_TAREA")
    public String getEstadoTarea() {
        return estadoTarea;
    }

    public void setEstadoTarea(String estadoTarea) {
        this.estadoTarea = estadoTarea;
    }

    @Basic
    @Column(name = "DESCRIP_TAREA")
    public String getDescripTarea() {
        return descripTarea;
    }

    public void setDescripTarea(String descripTarea) {
        this.descripTarea = descripTarea;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PryTareasXProyectoEntity that = (PryTareasXProyectoEntity) o;
        return Objects.equals(ideTareaPry, that.ideTareaPry) && Objects.equals(codigoTarea, that.codigoTarea) &&
                Objects.equals(horasEstimadas, that.horasEstimadas) && Objects.equals(estadoTarea, that.estadoTarea) &&
                Objects.equals(descripTarea,that.descripTarea);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ideTareaPry, codigoTarea, horasEstimadas, estadoTarea,descripTarea);
    }

    @ManyToOne
    @JoinColumn(name = "IDE_PROYECTO", referencedColumnName = "IDE_PROYECTO")
    public PryProyectoEntity getPryProyectoByIdeProyecto() {
        return pryProyectoByIdeProyecto;
    }

    public void setPryProyectoByIdeProyecto(PryProyectoEntity pryProyectoByIdeProyecto) {
        this.pryProyectoByIdeProyecto = pryProyectoByIdeProyecto;
    }
}
