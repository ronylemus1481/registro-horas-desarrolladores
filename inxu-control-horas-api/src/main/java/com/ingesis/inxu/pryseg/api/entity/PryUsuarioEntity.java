package com.ingesis.inxu.pryseg.api.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "PRY_USUARIO", schema = "PRYSEG")
@NamedQueries({
        @NamedQuery(name="PryUsuarioEntity.findAll",
                query = "SELECT r FROM PryUsuarioEntity r ORDER BY r.nombreUsuario"),

        @NamedQuery(name="PryUsuarioEntity.findAllByEstado",
                query = "SELECT r FROM PryUsuarioEntity r WHERE r.estadoUsuario = :estadoUsuario ORDER BY r.nombreUsuario")
})
public class PryUsuarioEntity implements Serializable {

    private Long ideUsuario;
    private String codUsuario;
    private String nombreUsuario;
    private String estadoUsuario;
    private String claveUsuario;

    @Id
    @Column(name = "IDE_USUARIO")
    public Long getIdeUsuario() {
        return ideUsuario;
    }

    public void setIdeUsuario(Long ideUsuario) {
        this.ideUsuario = ideUsuario;
    }

    @Basic
    @Column(name = "COD_USUARIO")
    public String getCodUsuario() {
        return codUsuario;
    }

    public void setCodUsuario(String codUsuario) {
        this.codUsuario = codUsuario;
    }

    @Basic
    @Column(name = "NOMBRE_USUARIO")
    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    @Basic
    @Column(name = "ESTADO_USUARIO")
    public String getEstadoUsuario() {
        return estadoUsuario;
    }

    public void setEstadoUsuario(String estadoUsuario) {
        this.estadoUsuario = estadoUsuario;
    }

    @Basic
    @Column(name = "CLAVE_USUARIO")
    public String getClaveUsuario() {
        return claveUsuario;
    }

    public void setClaveUsuario(String claveUsuario) {
        this.claveUsuario = claveUsuario;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PryUsuarioEntity that = (PryUsuarioEntity) o;
        return Objects.equals(ideUsuario, that.ideUsuario) && Objects.equals(codUsuario, that.codUsuario) && Objects.equals(nombreUsuario, that.nombreUsuario) && Objects.equals(estadoUsuario, that.estadoUsuario) && Objects.equals(claveUsuario, that.claveUsuario);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ideUsuario, codUsuario, nombreUsuario, estadoUsuario, claveUsuario);
    }
}
