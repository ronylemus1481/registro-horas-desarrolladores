package com.ingesis.inxu.pryseg.api.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "PRY_PROYECTO", schema = "PRYSEG")
@NamedQueries({
        @NamedQuery(name="PryProyectoEntity.findAll",
                query = "SELECT r FROM PryProyectoEntity r ORDER BY r.pryEmpresaByIdeEmpresa.nombreEmpresa, r.nombreProyecto"),

        @NamedQuery(name="PryProyectoEntity.findAllByIdeEmpresa",
                query = "SELECT r FROM PryProyectoEntity r WHERE r.pryEmpresaByIdeEmpresa = :ideEmpresa")
})

public class PryProyectoEntity implements Serializable {

    private Long ideProyecto;
    private String codigoProyecto;
    private String nombreProyecto;
    private BigDecimal horasCotizacion;
    private BigDecimal valorCotizacion;
    private String estadoProyecto;
    private PryEmpresaEntity pryEmpresaByIdeEmpresa;

    @Id
    @Column(name = "IDE_PROYECTO")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "genIdeProyecto")
    @TableGenerator(name = "genIdeProyecto", table = "pry_secuencia_tablas",
            pkColumnName = "cod_secuencia", pkColumnValue = "pry_proyecto",
            valueColumnName = "correlativo", initialValue = 1, allocationSize = 1)

    public Long getIdeProyecto() {
        return ideProyecto;
    }

    public void setIdeProyecto(Long ideProyecto) {
        this.ideProyecto = ideProyecto;
    }

    @Basic
    @Column(name = "CODIGO_PROYECTO")
    public String getCodigoProyecto() {
        return codigoProyecto;
    }

    public void setCodigoProyecto(String codigoProyecto) {
        this.codigoProyecto = codigoProyecto;
    }

    @Basic
    @Column(name = "NOMBRE_PROYECTO")
    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    @Basic
    @Column(name = "HORAS_COTIZACION")
    public BigDecimal getHorasCotizacion() {
        return horasCotizacion;
    }

    public void setHorasCotizacion(BigDecimal horasCotizacion) {
        this.horasCotizacion = horasCotizacion;
    }

    @Basic
    @Column(name = "VALOR_COTIZACION")
    public BigDecimal getValorCotizacion() {
        return valorCotizacion;
    }

    public void setValorCotizacion(BigDecimal valorCotizacion) {
        this.valorCotizacion = valorCotizacion;
    }

    @Basic
    @Column(name = "ESTADO_PROYECTO")
    public String getEstadoProyecto() {
        return estadoProyecto;
    }

    public void setEstadoProyecto(String estadoProyecto) {
        this.estadoProyecto = estadoProyecto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PryProyectoEntity that = (PryProyectoEntity) o;
        return Objects.equals(ideProyecto, that.ideProyecto) && Objects.equals(codigoProyecto, that.codigoProyecto) &&
                Objects.equals(nombreProyecto, that.nombreProyecto) && Objects.equals(horasCotizacion, that.horasCotizacion) &&
                Objects.equals(valorCotizacion, that.valorCotizacion) && Objects.equals(estadoProyecto, that.estadoProyecto) &&
                Objects.equals(pryEmpresaByIdeEmpresa,that.pryEmpresaByIdeEmpresa);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ideProyecto, codigoProyecto, nombreProyecto, horasCotizacion, valorCotizacion, estadoProyecto,
                pryEmpresaByIdeEmpresa);
    }

    @ManyToOne
    @JoinColumn(name = "IDE_EMPRESA", referencedColumnName = "IDE_EMPRESA")
    public PryEmpresaEntity getPryEmpresaByIdeEmpresa() {
        return pryEmpresaByIdeEmpresa;
    }

    public void setPryEmpresaByIdeEmpresa(PryEmpresaEntity pryEmpresaByIdeEmpresa) {
        this.pryEmpresaByIdeEmpresa = pryEmpresaByIdeEmpresa;
    }
}
