package com.ingesis.inxu.pryseg.boundary;

import com.ingesis.inxu.pryseg.api.entity.PryProyectoEntity;
import com.ingesis.inxu.pryseg.api.entity.PryTareasXProyectoEntity;
import com.ingesis.inxu.pryseg.control.TareasPorProyecto;
import com.ingesis.inxu.pryseg.dao.TareasPorProyectoDao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import java.util.List;

@Stateless(name = "tareasPorProyectoService")
@LocalBean
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class TareasPorProyectoService {

    @Inject
    private TareasPorProyecto tareasPorProyecto;

    @Inject
    private TareasPorProyectoDao tareasPorProyectoDao;

    public List<PryTareasXProyectoEntity> recuperarTareasPorProyecto(PryProyectoEntity proyectoEntity){
        return tareasPorProyecto.recuperarTareasPorProyecto(proyectoEntity);
    }
    public List<PryTareasXProyectoEntity> recuperarTareasPorEmpresa(Long ideEmpresa){
        return tareasPorProyectoDao.recuperarTareasPorEmpresa(ideEmpresa);
    }

    public List<PryTareasXProyectoEntity> recuperarTodasLasTareas(){
        return tareasPorProyectoDao.recuperarTodasLasTareas();
    }

    public Object recuperarTareaPorProyectoByReference(Long ideTareaPry) {
        return tareasPorProyecto.recuperarTareaPorProyectoByReference(ideTareaPry);
    }

    public void crearTarea(PryTareasXProyectoEntity tareasXProyectoEntity){
        tareasPorProyectoDao.create(tareasXProyectoEntity);
    }

    public void modificarTarea(PryTareasXProyectoEntity tareasXProyectoEntity){
        tareasPorProyectoDao.update(tareasXProyectoEntity);
    }

    public void eliminarTarea(PryTareasXProyectoEntity tareasXProyectoEntity){
        tareasPorProyectoDao.remove(tareasXProyectoEntity);
    }


}
