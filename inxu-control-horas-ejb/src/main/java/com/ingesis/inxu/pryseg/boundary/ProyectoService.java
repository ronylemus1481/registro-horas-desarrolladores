package com.ingesis.inxu.pryseg.boundary;


import com.ingesis.inxu.pryseg.api.entity.PryEmpresaEntity;
import com.ingesis.inxu.pryseg.api.entity.PryProyectoEntity;
import com.ingesis.inxu.pryseg.control.Proyecto;
import com.ingesis.inxu.pryseg.dao.ProyectoDAO;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import java.util.List;

@Stateless(name = "proyectoService")
@LocalBean
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class ProyectoService {

    @Inject
    private Proyecto proyecto;

    @Inject
    private ProyectoDAO proyectoDAO;

    public List<PryProyectoEntity> recuperarProyectosPorEmpresa(PryEmpresaEntity pryEmpresaEntity){
        return proyecto.recuperarProyectosPorEmpresa(pryEmpresaEntity);
    }

    public Object recuperarProyectoByReference(Long ideProyecto){
        return proyecto.recuperarProyectoByReference(ideProyecto);
    }

    public void crearProyecto(PryProyectoEntity proyectoEntity){
        proyecto.crearProyecto(proyectoEntity);
    }

    public void modificarProyecto(PryProyectoEntity proyectoEntity){
            proyecto.modificaProyecto(proyectoEntity);
    }

    public void eliminarProyecto(PryProyectoEntity proyectoEntity){
        proyectoDAO.remove(proyectoEntity);
    }

    public List<PryProyectoEntity> recuperarTodosLosProyectos(){
        return proyectoDAO.recuperarTodosLosProyectos();
    }
}
