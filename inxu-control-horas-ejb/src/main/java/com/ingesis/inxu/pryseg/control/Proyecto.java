package com.ingesis.inxu.pryseg.control;

import com.ingesis.inxu.pryseg.api.entity.PryEmpresaEntity;
import com.ingesis.inxu.pryseg.api.entity.PryProyectoEntity;
import com.ingesis.inxu.pryseg.dao.ProyectoDAO;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
public class Proyecto {

    @Inject
    private ProyectoDAO proyectoDAO;


    public List<PryProyectoEntity> recuperarProyectosPorEmpresa(PryEmpresaEntity pryEmpresaEntity){
        return proyectoDAO.recuperarProyectosPorEmpresa(pryEmpresaEntity);
    }

    public Object recuperarProyectoByReference(Long ideProyecto){
        return proyectoDAO.getReference(ideProyecto);
    }

    public void crearProyecto(PryProyectoEntity proyectoEntity){
        proyectoDAO.create(proyectoEntity);
    }

    public void modificaProyecto(PryProyectoEntity proyectoEntity){
        proyectoDAO.update(proyectoEntity);
    }
}
