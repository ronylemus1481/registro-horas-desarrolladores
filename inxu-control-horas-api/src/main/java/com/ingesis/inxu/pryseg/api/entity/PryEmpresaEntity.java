package com.ingesis.inxu.pryseg.api.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "PRY_EMPRESA", schema = "PRYSEG")
@NamedQueries({
        @NamedQuery(name="PryEmpresaEntity.findAll",
                query = "SELECT r FROM PryEmpresaEntity r ORDER BY r.nombreEmpresa"),

        @NamedQuery(name="PryEmpresaEntity.findAllByEstado",
                query = "SELECT r FROM PryEmpresaEntity r WHERE r.estadoEmpresa = :estadoEmpresa ORDER BY r.nombreEmpresa")
})

public class PryEmpresaEntity implements Serializable {

    private Long ideEmpresa;
    private String codigoEmpresa;
    private String nombreEmpresa;
    private String estadoEmpresa;
    private List<PryProyectoEntity> pryProyectoEntityList;

    @Id
    @Column(name = "IDE_EMPRESA")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "genIdeEmpresa")
    @TableGenerator(name = "genIdeEmpresa", table = "pry_secuencia_tablas",
            pkColumnName = "cod_secuencia", pkColumnValue = "pry_empresa",
            valueColumnName = "correlativo", initialValue = 1, allocationSize = 1)
    public Long getIdeEmpresa() {
        return ideEmpresa;
    }

    public void setIdeEmpresa(Long ideEmpresa) {
        this.ideEmpresa = ideEmpresa;
    }

    @Basic
    @Column(name = "CODIGO_EMPRESA")
    public String getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(String codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    @Basic
    @Column(name = "NOMBRE_EMPRESA")
    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    @Basic
    @Column(name = "ESTADO_EMPRESA")
    public String getEstadoEmpresa() {
        return estadoEmpresa;
    }

    public void setEstadoEmpresa(String estadoEmpresa) {
        this.estadoEmpresa = estadoEmpresa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PryEmpresaEntity that = (PryEmpresaEntity) o;
        return Objects.equals(ideEmpresa, that.ideEmpresa) && Objects.equals(codigoEmpresa, that.codigoEmpresa) && Objects.equals(nombreEmpresa, that.nombreEmpresa) && Objects.equals(estadoEmpresa, that.estadoEmpresa);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ideEmpresa, codigoEmpresa, nombreEmpresa, estadoEmpresa);
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pryEmpresaByIdeEmpresa")
    public List<PryProyectoEntity> getPryProyectoEntityList() {
        return pryProyectoEntityList;
    }

    public void setPryProyectoEntityList(List<PryProyectoEntity> pryProyectoEntityList) {
        this.pryProyectoEntityList = pryProyectoEntityList;
    }
}
