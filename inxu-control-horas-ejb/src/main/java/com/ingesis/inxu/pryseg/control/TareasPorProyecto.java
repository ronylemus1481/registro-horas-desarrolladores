package com.ingesis.inxu.pryseg.control;

import com.ingesis.inxu.pryseg.api.entity.PryProyectoEntity;
import com.ingesis.inxu.pryseg.api.entity.PryTareasXProyectoEntity;
import com.ingesis.inxu.pryseg.dao.TareasPorProyectoDao;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
public class TareasPorProyecto {

    @Inject
    private TareasPorProyectoDao tareasPorProyectoDao;

    public List<PryTareasXProyectoEntity> recuperarTareasPorProyecto(PryProyectoEntity proyectoEntity){
        return tareasPorProyectoDao.recuperarTareasPorProyecto(proyectoEntity);
    }

    public Object recuperarTareaPorProyectoByReference(Long ideTareaPry) {
        return tareasPorProyectoDao.getReference(ideTareaPry);
    }

}
