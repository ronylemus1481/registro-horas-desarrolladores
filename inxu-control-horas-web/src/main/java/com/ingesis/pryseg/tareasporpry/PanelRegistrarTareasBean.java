package com.ingesis.pryseg.tareasporpry;

import com.ingesis.inxu.pryseg.api.entity.PryEmpresaEntity;
import com.ingesis.inxu.pryseg.api.entity.PryProyectoEntity;
import com.ingesis.inxu.pryseg.api.entity.PryTareasXProyectoEntity;
import com.ingesis.inxu.pryseg.boundary.EmpresaService;
import com.ingesis.inxu.pryseg.boundary.ProyectoService;
import com.ingesis.inxu.pryseg.boundary.TareasPorProyectoService;
import com.ingesis.inxu.pryseg.control.EstadosConstantes;
import com.ingesis.inxu.pryseg.util.FacesUtil;
import com.ingesis.pryseg.comunes.ComunEstados;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


@ManagedBean
@ViewScoped
public class PanelRegistrarTareasBean implements Serializable {

    private Long ideTareaPry;
    private Long ideProyecto;
    private Long ideEmpresa;
    private String codigoTarea;
    private String estadoTarea;
    private String descripTarea;
    private BigDecimal horasEstimadas;

    private List<SelectItem> empresasOptions;
    private List<SelectItem> proyectosOptions;
    private List<SelectItem> estadosOptions;

    private List<PryTareasXProyectoEntity> tareasXProyectoEntityList;

    @EJB
    private EmpresaService empresaService;
    @EJB
    private ProyectoService proyectoService;
    @EJB
    private TareasPorProyectoService tareasPorProyectoService;

    @PostConstruct
    public void init(){
        llenarListaEmpresas();
        llenarEstadosOptions();
    }

    public void agregarTarea(){
        PryTareasXProyectoEntity tareasXProyectoEntity = new PryTareasXProyectoEntity();

        PryProyectoEntity proyectoEntity = (PryProyectoEntity) proyectoService.recuperarProyectoByReference(ideProyecto);
        tareasXProyectoEntity.setPryProyectoByIdeProyecto(proyectoEntity);
        tareasXProyectoEntity.setCodigoTarea(codigoTarea);
        tareasXProyectoEntity.setEstadoTarea(estadoTarea);
        tareasXProyectoEntity.setDescripTarea(descripTarea);
        tareasXProyectoEntity.setHorasEstimadas(horasEstimadas);

        if (this.ideTareaPry != null){
            tareasXProyectoEntity.setIdeTareaPry(ideTareaPry);
        }

        grabarTarea(tareasXProyectoEntity);
        aplicarFiltro();
        new FacesUtil().addInfoMessage("Registro de la tarea grabado satisfactoriamente.");

    }

    public void asignarTareaParaModificar(PryTareasXProyectoEntity tareasXProyectoEntity){
        this.ideTareaPry = tareasXProyectoEntity.getIdeTareaPry();
        this.ideEmpresa = tareasXProyectoEntity.getPryProyectoByIdeProyecto().getPryEmpresaByIdeEmpresa().getIdeEmpresa();
        this.ideProyecto = tareasXProyectoEntity.getPryProyectoByIdeProyecto().getIdeProyecto();
        this.codigoTarea = tareasXProyectoEntity.getCodigoTarea();
        this.descripTarea = tareasXProyectoEntity.getDescripTarea();
        this.horasEstimadas = tareasXProyectoEntity.getHorasEstimadas();
        this.estadoTarea = tareasXProyectoEntity.getEstadoTarea();

        llenarListaProyectosPorEmpresa();
    }

    public void eliminarTarea(PryTareasXProyectoEntity tareasXProyectoEntity){
        tareasPorProyectoService.eliminarTarea(tareasXProyectoEntity);
        aplicarFiltro();
        new FacesUtil().addInfoMessage("Registro de la tarea eliminado satisfactoriamente.");
    }

    public void inicializarPanelTareas(){
        this.ideTareaPry = null;
        this.ideEmpresa = null;
        this.ideProyecto = null;
        this.codigoTarea = null;
        this.descripTarea = null;
        this.horasEstimadas = null;
        this.estadoTarea = null;
        this.proyectosOptions = new ArrayList<>();
        tareasXProyectoEntityList = new ArrayList<>();
        llenarListaEmpresas();
    }

    private void grabarTarea(PryTareasXProyectoEntity tareasXProyectoEntity){
        if (tareasXProyectoEntity.getIdeTareaPry() == null){
            tareasPorProyectoService.crearTarea(tareasXProyectoEntity);
        }else {
            tareasPorProyectoService.modificarTarea(tareasXProyectoEntity);
        }
    }

    public void llenarListaEmpresas(){
        List<PryEmpresaEntity> list = empresaService.recuperarEmpresasPorEstado(EstadosConstantes.ACTIVO);
        empresasOptions = new ArrayList<>();

        for (PryEmpresaEntity empresaEntity : list){
            empresasOptions.add(new SelectItem(empresaEntity.getIdeEmpresa(),empresaEntity.getCodigoEmpresa()
                    + "-"+empresaEntity.getNombreEmpresa()));
        }

    }

    public void llenarListaProyectosPorEmpresa(){
        PryEmpresaEntity empresaEntity = (PryEmpresaEntity) empresaService.recuperarEmpresaPorReferencia(ideEmpresa);
        List<PryProyectoEntity> list = proyectoService.recuperarProyectosPorEmpresa(empresaEntity);
        proyectosOptions = new ArrayList<>();
        for (PryProyectoEntity proyectoEntity : list){
            proyectosOptions.add(new SelectItem(proyectoEntity.getIdeProyecto(),proyectoEntity.getNombreProyecto()));
        }
    }

    private void llenarEstadosOptions(){
        estadosOptions = new ComunEstados().listaDeEstados();
    }

    public void aplicarFiltro(){
        PryProyectoEntity proyectoEntity = new PryProyectoEntity();
        proyectoEntity.setIdeProyecto(ideProyecto);

        if (ideEmpresa != null && ideProyecto == null){
            tareasXProyectoEntityList = tareasPorProyectoService.recuperarTareasPorEmpresa(ideEmpresa);
        }else if (ideEmpresa != null && ideProyecto != null) {
            tareasXProyectoEntityList = tareasPorProyectoService.recuperarTareasPorProyecto(proyectoEntity);
        }else {
            tareasXProyectoEntityList = tareasPorProyectoService.recuperarTodasLasTareas();
        }
    }

    public Long getIdeTareaPry() {
        return ideTareaPry;
    }

    public void setIdeTareaPry(Long ideTareaPry) {
        this.ideTareaPry = ideTareaPry;
    }

    public Long getIdeProyecto() {
        return ideProyecto;
    }

    public void setIdeProyecto(Long ideProyecto) {
        this.ideProyecto = ideProyecto;
    }

    public Long getIdeEmpresa() {
        return ideEmpresa;
    }

    public void setIdeEmpresa(Long ideEmpresa) {
        this.ideEmpresa = ideEmpresa;
    }

    public String getCodigoTarea() {
        return codigoTarea;
    }

    public void setCodigoTarea(String codigoTarea) {
        this.codigoTarea = (codigoTarea != null ? codigoTarea.toUpperCase() : null);
    }

    public String getEstadoTarea() {
        return estadoTarea;
    }

    public void setEstadoTarea(String estadoTarea) {
        this.estadoTarea = estadoTarea;
    }

    public String getDescripTarea() {
        return descripTarea;
    }

    public void setDescripTarea(String descripTarea) {
        this.descripTarea = (descripTarea != null ? descripTarea.toUpperCase() : null);
    }

    public BigDecimal getHorasEstimadas() {
        return horasEstimadas;
    }

    public void setHorasEstimadas(BigDecimal horasEstimadas) {
        this.horasEstimadas = horasEstimadas;
    }

    public List<SelectItem> getEmpresasOptions() {
        return empresasOptions;
    }

    public void setEmpresasOptions(List<SelectItem> empresasOptions) {
        this.empresasOptions = empresasOptions;
    }

    public List<SelectItem> getProyectosOptions() {
        return proyectosOptions;
    }

    public void setProyectosOptions(List<SelectItem> proyectosOptions) {
        this.proyectosOptions = proyectosOptions;
    }

    public List<SelectItem> getEstadosOptions() {
        return estadosOptions;
    }

    public void setEstadosOptions(List<SelectItem> estadosOptions) {
        this.estadosOptions = estadosOptions;
    }

    public List<PryTareasXProyectoEntity> getTareasXProyectoEntityList() {
        return tareasXProyectoEntityList;
    }

    public void setTareasXProyectoEntityList(List<PryTareasXProyectoEntity> tareasXProyectoEntityList) {
        this.tareasXProyectoEntityList = tareasXProyectoEntityList;
    }
}
