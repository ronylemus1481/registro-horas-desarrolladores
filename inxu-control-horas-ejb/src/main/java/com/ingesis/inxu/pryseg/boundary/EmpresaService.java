package com.ingesis.inxu.pryseg.boundary;

import com.ingesis.inxu.pryseg.api.entity.PryEmpresaEntity;
import com.ingesis.inxu.pryseg.control.Empresa;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import java.util.List;

@Stateless(name = "empresaService")
@LocalBean
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class EmpresaService {

    @Inject
    private Empresa empresa;


    public List<PryEmpresaEntity> recuperarEmpresasPorEstado(String estado){
        return empresa.recuperarEmpresasPorEstado(estado);
    }

    public Object recuperarEmpresaPorReferencia(Long ideEmpresa){
        return empresa.recuperarEmpresaPorReferencia(ideEmpresa);
    }

    public void crearEmpresa(PryEmpresaEntity pryEmpresaEntity){
        empresa.crearEmpresa(pryEmpresaEntity);
    }

    public void modificarEmpresa(PryEmpresaEntity pryEmpresaEntity){
        empresa.modificarEmpresa(pryEmpresaEntity);
    }

    public void eliminarEmpresa(PryEmpresaEntity pryEmpresaEntity){
        empresa.eliminarEmpresa(pryEmpresaEntity);
    }


}
