package com.ingesis.inxu.pryseg.control;

import com.ingesis.inxu.pryseg.api.entity.PryEmpresaEntity;
import com.ingesis.inxu.pryseg.dao.EmpresaDAO;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
public class Empresa {

    @Inject
    private EmpresaDAO empresaDAO;

    public List<PryEmpresaEntity> recuperarEmpresasPorEstado(String estado){
        return empresaDAO.recuperarEmpresasPorEstado(estado);
    }

    public Object recuperarEmpresaPorReferencia(Long ideEmpresa){
        return empresaDAO.getReference(ideEmpresa);
    }

    public void crearEmpresa(PryEmpresaEntity pryEmpresaEntity){
        empresaDAO.create(pryEmpresaEntity);
    }

    public void modificarEmpresa(PryEmpresaEntity pryEmpresaEntity){
        empresaDAO.update(pryEmpresaEntity);
    }

    public void eliminarEmpresa(PryEmpresaEntity pryEmpresaEntity){
        empresaDAO.remove(pryEmpresaEntity);
    }

}
